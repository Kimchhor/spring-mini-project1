package com.ksga.springminiproject.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostModel {
    private int id;
    private String title;
    private String description;
    private String image;
    private String comment;
}
