package com.ksga.springminiproject.repository;

import com.ksga.springminiproject.model.PostModel;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostModelRepository {
    @Select("select * from tb_post")
    public List<PostModel> findAll();
}
