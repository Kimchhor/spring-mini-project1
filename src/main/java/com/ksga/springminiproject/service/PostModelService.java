package com.ksga.springminiproject.service;

import com.ksga.springminiproject.model.PostModel;
import com.ksga.springminiproject.repository.PostModelRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface PostModelService {


    List<PostModel> findAll();
}
