package com.ksga.springminiproject.service.imp;

import com.ksga.springminiproject.model.PostModel;
import com.ksga.springminiproject.repository.PostModelRepository;
import com.ksga.springminiproject.service.PostModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostModelServiceImp implements PostModelService {

    private final PostModelRepository postModelRepository;

    @Autowired
    public PostModelServiceImp(PostModelRepository postModelRepository) {
        this.postModelRepository = postModelRepository;
    }

    @Override
    public List<PostModel> findAll() {
        return postModelRepository.findAll();
    }
}
