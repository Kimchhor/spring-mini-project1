package com.ksga.springminiproject.controller;

import com.ksga.springminiproject.service.PostModelService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RedditController {

    private final PostModelService postModelService;

    public RedditController(PostModelService postModelService) {
        this.postModelService = postModelService;
    }

    @GetMapping("/home")
    public String home(ModelMap modelMap){
        modelMap.addAttribute("postModel", postModelService.findAll());
        System.out.println(postModelService.findAll());
        return "index";
    }
    @GetMapping("/create_post")
    public String create_post (){
        return "create_post";
    }
    @GetMapping("/createSubReddit")
    public String subReddit(){
        return "createSubReddit";
    }


}
